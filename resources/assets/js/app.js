import router from './router'
import store from './vuex'
import Vue from 'vue'

require('./bootstrap.js');

Vue.component('app', require('./components/App.vue'));

new Vue({
    el: '#app',
    router: router,
    store: store
});