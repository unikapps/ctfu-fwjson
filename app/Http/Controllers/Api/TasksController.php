<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\TaskStoreRequest;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TasksController extends Controller
{
    /**
     * Display the list of Tasks
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $tasks = Task::orderBy('priority');

        if ($request->project){
            $tasks= $tasks->where('project_id', $request->project);
        }

        $tasks= $tasks->paginate(15);

        $projects = Project::all();
        return view('tasks.index', compact('tasks', 'projects'));
    }

    /**
     * Display the form to create new Task with the projects list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $projects = Project::all();

        return view('tasks.create', compact('projects'));
    }

    /**
     * Store submitted Task after validating the form data
     *
     * @param TaskStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TaskStoreRequest $request)
    {
        $task             = new Task;
        $task->name       = $request->name;
        $task->priority   = $request->priority;
        $task->project_id = $request->project_id;

        if ($task->save()) {
            return back();
        }
    }

    /**
     * Display the page to edit the task
     * @param $id
     *
     */
    public function edit($id)
    {
        $task     = Task::with('project')->where('id', $id)->first();
        $projects = Project::all();

        return response()->json([
            'tasks'
        ]);
    }

    /**
     * Update the task
     *
     * @param TaskStoreRequest $request
     * @param Task $task
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TaskStoreRequest $request, Task $task)
    {
        $taskData = [
            'name'       => $request->get('name', $task->name),
            'project_id' => $request->get('project_id', $task->project_id),
            'priority'   => $request->get('priority', $task->priority),
        ];

        $task->update($taskData);

    }

    /**
     * Destroy the task and go back to tasks list
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Task::find($id)->delete();

        return back();
    }

    /**
     * Re-order tasks
     * @todo: enqueue this operation if the list of tasks  is big
     *
     * @param Request $request
     *
     * @return array
     */
    public function reOrder(Request $request)
    {
        foreach ($request->tasks as $priority => $id) {
            Task::find($id)->update(['priority' => $priority + 1]);
        }

        return response()->json([
            'data' => [
                'status' => 'successfully reordered the tasks',
            ],
        ], 200);
    }
}
