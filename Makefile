#
#--------------------------------------------------------------------------
# Laradock (Docker) commands
#--------------------------------------------------------------------------
#
# Here we can group all laradock command to start, stop, restart shutdon
# or  destroy containers ect, the file can also contain any additional
# system commands or aliases we use frequently in the project
#
#

CONTAINERS=nginx
LARADOCK_PATH=../laradock
PROJECT_FOLDER=CTFollowUp

up:
	cd ${LARADOCK_PATH} && \
	docker-compose up -d  ${CONTAINERS}

start:
	cd ${LARADOCK_PATH} && \
	docker-compose start ${CONTAINERS}

# Stop All running containers, append ${CONTAINERS} to specify the containers
stop:
	cd ${LARADOCK_PATH} && \
	docker-compose stop

#Restart containers
restart:
	make stop && make up

ps:
	cd ${LARADOCK_PATH} && \
	docker-compose ps

# Remove All containers, append ${CONTAINERS} to specify the containers
down:
	cd ${LARADOCK_PATH} && \
	docker-compose down

# SSH into workspace
ws:
	docker exec -it \
		-w /var/www/${PROJECT_FOLDER} \
		laradock_workspace_1 \
		bash

# SSH into php-worker container
worker:
	cd ${LARADOCK_PATH} && \
	docker-compose exec \
		php-worker \
		bash

# SSH into redis container
redis:
	cd ${LARADOCK_PATH} && \
	docker-compose exec \
		redis \
		bash

# run laradock/mysql/docker-entrypoint-initdb.d/createdb.sql
# too create the database if needed
createdb:
	cd ${LARADOCK_PATH} && \
	docker-compose exec \
	mysql \
	mysql -uroot -proot \
	-e "source /docker-entrypoint-initdb.d/createdb.sql"

# SSH into mysql container
mysql:
	cd ${LARADOCK_PATH} && \
	docker-compose exec \
	mysql \
	bash

#Run artisan command into VM
cmd=""
art:
	cd ${LARADOCK_PATH} && \
	docker-compose exec -u root\
		php-fpm \
		php artisan $(cmd) \
        2>/dev/null || true

